# -*- coding: utf-8 -*-
# simple_registration_system/tests.py


from __future__ import unicode_literals

from django.test import TestCase
from django.urls import reverse_lazy


class LoginTestCase(TestCase):

    def test_signup_page(self):
        # Load sign up page.
        response = self.client.get(reverse_lazy(u'signup'))

        # Check sign page loaded.
        self.assertTemplateUsed('sign.html')
        self.assertEqual(response.status_code, 200)


    def test_signup(self):
        # Create a new user.
        response = self.client.post(reverse_lazy(u'signup'), { u'username': u'dummy', u'password': u'dummy'})

        # Check user is logged and home page loaded.
        self.assertIn('_auth_user_id', self.client.session)
        self.assertTemplateUsed('home.html')
        self.assertRedirects(response, reverse_lazy(u'home'))


    def test_signup_page(self):
        # Load login page.
        response = self.client.get(reverse_lazy(u'login'))

        # Check sign page loaded.
        self.assertTemplateUsed('sign.html')
        self.assertEqual(response.status_code, 200)


    def test_login(self):
        # Create a new user.
        response = self.client.post(reverse_lazy(u'signup'), { u'username': u'dummy', u'password': u'dummy'})
        self.assertIn('_auth_user_id', self.client.session)
        # Logout with the user.
        response = self.client.get(reverse_lazy(u'logout'))
        self.assertNotIn('_auth_user_id', self.client.session)
        # Login the user.
        response = self.client.post(reverse_lazy(u'login'), { u'username': u'dummy', u'password': u'dummy'})

        # Check user is logged and home page loaded.
        self.assertIn('_auth_user_id', self.client.session)
        self.assertTemplateUsed('home.html')
        self.assertRedirects(response, reverse_lazy(u'home'))


    def test_home_not_logged(self):
        # Check not logged users.
        self.assertNotIn('_auth_user_id', self.client.session)

        # Check access to home page redirects to login page.
        response = self.client.get(reverse_lazy(u'home'))
        self.assertRedirects(response, reverse_lazy(u'login')+u'?next=/')


    def test_home_logged(self):
        # Create a new user.
        response = self.client.post(reverse_lazy(u'signup'), { u'username': u'dummy', u'password': u'dummy'})

        # Check user is logged.
        self.assertIn('_auth_user_id', self.client.session)

        # Check access to home page doesn't redirect to login page.
        response = self.client.get(reverse_lazy(u'home'))
        self.assertTemplateUsed('home.html')
        self.assertEqual(response.status_code, 200)
