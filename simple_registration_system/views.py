# -*- coding: utf-8 -*-
# simple_registration_system/views.py


from django.urls import reverse_lazy
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.views.generic import TemplateView, CreateView, FormView
from django.views.generic.base import RedirectView
from django.contrib.auth.mixins import LoginRequiredMixin



class HomeView(LoginRequiredMixin, TemplateView):
    '''
    Home view (Only accessible if logged).

    Attributes:
        template_name (str): Template to be rendered.
        login_url (str): URL of the login page to be redirected in case of not logged.
    '''
    template_name = u'home.html'
    login_url = reverse_lazy(u'login')



class SignUpView(CreateView):
    '''
    Sign Up view, with a registration form to create new users.

    Attributes:
        template_name (str): Template to be rendered.
        model (Model): Model to be created.
        fields (list): List of model's fields to be rendered in the form.
        success_url (str): URL to be redirected when the form is success.
    '''
    template_name = u'sign.html'
    model = User
    fields = [u'username', u'password' ]
    success_url = reverse_lazy(u'home')

    def form_valid(self, form):
        '''
        Make actions and redirects to get_success_url()
        '''
        username = form.cleaned_data[u'username']
        password = form.cleaned_data[u'password']

        user = form.save(commit=False)
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)

        if user:
            login(self.request, user)

        return super(SignUpView, self).form_valid(form)


    def get_context_data(self, **kwargs):
        '''
        Returns a dictionary representing the template context.
        '''
        context = super(SignUpView, self).get_context_data(**kwargs)
        context[u'submit_text'] = u'Sign Up'

        return context



class LoginView(FormView):
    '''
    Login view, with a login form to authenticate users.

    Attributes:
        template_name (str): Template to be rendered.
        form_class (Form): Form to be rendered.
        success_url (str): URL to be redirected when the form is success.
    '''
    template_name = u'sign.html'
    form_class = AuthenticationForm
    success_url = reverse_lazy(u'home')

    def form_valid(self, form):
        '''
        Make actions and redirects to get_success_url()
        '''
        login(self.request, form.get_user())

        return super(LoginView, self).form_valid(form)


    def get_context_data(self, **kwargs):
        '''
        Returns a dictionary representing the template context.
        '''
        context = super(LoginView, self).get_context_data(**kwargs)
        context[u'submit_text'] = u'Login'

        return context



class LogoutView(RedirectView):
    '''
    Logout view, which logout the authenticated user.

    Attributes:
        url (str): URL to be redirected when the user is logged out.
    '''
    url = reverse_lazy(u'home')

    def get(self, request, *args, **kwargs):
        '''
        Logout the authenticated user.
        '''
        logout(request)

        return super(LogoutView, self).get(request, *args, **kwargs)
